# path is native to django and allows you to establish
# a url pattern. The first part is an argument where you establish the
# input for the path, the second is the function that you created for
# redirecting the path, the last is the name assigned to the path so you can
# import and call it in other python modules
from django.urls import path

# this imports functions from your views.py to pass through the urlpatterns
from todos.views import todo_list, todo_list_detail, todo_list_create, todo_list_update, todo_list_delete


# url patterns is a list of paths to help redirect the user when input
# is provided
# path(location/html path, views function that was imported, name=usually same name as views function)
urlpatterns = [
    path("todos/", todo_list, name="todo_list_list"),
    path("todos/<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("todos/create/", todo_list_create, name="todo_list_create"),
    path("todos/<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("todos/<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
]
# MAKE SURE SYNTAX IS CORRECT NAME = "" DO NOT PASS IN STRING FOR 3RD ARGUMENT
