# import a built in template from django that is for creating forms
from django.forms import ModelForm

# call the todo list model you made for entering fields into the django recipe app
from todos.models import TodoList


# create a class called TodoList to allow users to create todo lists
class TodoListForm(ModelForm):
    # meta is a subclass within a class
    class Meta:
        # use the todolist form made in the models.py and use those fields in the template
        # only add the fields that people should be able to create so not datetime etc.
        model = TodoList
        fields = ("name",)
