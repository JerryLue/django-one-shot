from django.contrib import admin

# import your models here
from todos.models import TodoList, TodoItem

# Register your models here.


# Register the todolist model with a name and id as columns
@admin.register(TodoList)
# creating an admin class using the django model framework
class TodoListAdmin(admin.ModelAdmin):
    # What columns to display in the Todo List table in admin page
    list_display = (
        # make a name column
        "name",
        # make an id column
        "id",
        # date created is automatic and should not be changed even by the admin
    )


# register todoitem model in the admin
@admin.register(TodoItem)
# creates an admin class which will allow you to edit these fields in the local host admin page
class TodoItemAdmin(admin.ModelAdmin):
    # creates a list for columns to be displayed in the local host admin page
    # list display items can be referenced in the models.py file
    list_display = (
        # creates a task column
        "task",
        # creates a due_date column
        "due_date",
        # creates a is_completed column
        "is_completed",
    )
