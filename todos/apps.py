from django.apps import AppConfig


# use the class name to install app to settings.py in the project
class TodosConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "todos"
