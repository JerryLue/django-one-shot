from django.shortcuts import render, get_object_or_404, redirect

# imports models you created in models.py and allows you to access the model class contents
from todos.models import TodoItem, TodoList

# imports forms made in forms.py
from todos.form import TodoListForm


# Create your views here.


# define a view to see your todo list
def todo_list(request):
    # sets list equal to all the objects in the todo list
    lists = TodoList.objects.all()
    # creates context dictionary assigning these objects to todo_list_list
    context = {
        "todo_list_list": lists,
    }
    # returns a rendered version with the context data in the list.html

    return render(request, "todos/list.html", context)


# define a view that shows the details of a todo list including
# a list of todo items
def todo_list_detail(request, id):
    # sets variable detail to the TodoList with id set to the id of what the user clicked
    detail = TodoList.objects.get(id=id)
    # pass this detail variable into the model and use it to fetch all TodoItems
    # to the todolist object
    context = {
        "todo_list_detail": detail,
    }
    # returns a rendered version with the context data in the detail.html
    return render(request, "todos/detail.html", context)


# define a view for TodoList model that will show the name field in the form and handle the
# form submission to create a new TodoList
##people should be logged in to create recipe
##@login_required
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            ##if you want to add an author to the todo list
            ##set list to the unsaved form and
            # @list = form.save(False)
            ##add the author to the unsaved instance of the form
            # @list.author = request.user
            # save the form as list variable
            list = form.save()
            # redirect to the list page
            return redirect("todo_list_detail", id=list.id)
    # if form is not valid
    else:
        # resets form to the todo list form
        form = TodoListForm()
    # sets context dictionary with "form" equal to the user submitted form
    context = {
        "form": form,
    }
    # renders a new todolist item using the context and request
    return render(request, "todos/create.html", context)


# define an update view for the TodoList model that will show the name. field in the form
# and hangle the form submission to change an existing todo list
def todo_list_update(request, id):
    # Get the todolist object that we want to edit using id
    update = TodoList.objects.get(id=id)
    # checks if the request method is post which it should be since
    # your update.html button uses the post method
    if request.method == "POST":
        # POST is when the person has submitted the form or clicks enter
        # sets variable form equal to the Todolistform class defined in form.py
        # and passes in the request type and the todolist id
        # this tells form.py that you want to edit/post the todolist specified using its id
        form = TodoListForm(request.POST, instance=update)
        # use the form to validate the values
        # checks if the request method is valid and that the todolist/id matches what is
        # in our database. Has to match the format allowed by form.py
        # .is_valid is a django command and you can ctrl click to see the function
        if form.is_valid():
            # if everything is valid then save them to the database
            edit = form.save()
            # redirect back to the page that shows the new todolist using its id
            return redirect("todo_list_detail", id=edit.id)
    else:
        # if the request is not post then set form equal to the class todolistform passing
        # in update as the instance (nothing is changed and it should display the current recipe)
        # since there is not form validity check and no form save this will not change any info
        form = TodoListForm(instance=update)
    # context is a dictionary you are using to set values from this instance of the function into
    # variables that can be passed to html in the app
    context = {
        ## edit_object is used to identify which todo page to navigate to
        ##"edit_object": update,
        # form contains the edit response if the request was valid it will be the user input
        # else form will be equal to what it was before
        "form": form,
    }
    # returns to the edit page passing in the context this will not redirect should only
    # happen if the form is not valid basically resets the edit page since the input was not valid
    return render(request, "todos/edit.html", context)


# define a function to delete todo list
def todo_list_delete(request, id):
    to_be_deleted = TodoList.objects.get(id=id)
    if request.method == "POST":
        to_be_deleted.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


##code debugger put next to problem line run the server and use documentation to dubug
##import pdb
##print("hello")
##pdb.set_trace()
