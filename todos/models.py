from django.db import models


# Create your models here.
# define a class for a TodoList with attributes name and created on
class TodoList(models.Model):
    # name field with max 100 characters
    name = models.CharField(max_length=100)
    # immutable date created field set to automatically record datetime when called
    created_on = models.DateTimeField(auto_now=True)


# define a class for TodoItems to add a description on the TodoList
class TodoItem(models.Model):
    # task field with max length 100 characters
    task = models.CharField(max_length=100)
    # makes an optional due date datetime field
    due_date = models.DateTimeField(
        # sets the value to null and will auto populate with
        # blank if there are already tasks created
        null=True,
        blank=True,
    )
    # make a true/false boolean field to show if the task is completed
    is_completed = models.BooleanField(default=False)
    # links todolist with the todo item using a foreign key to access the model
    list = models.ForeignKey(
        # the model that we are referencing
        TodoList,
        # assigns a related name we can use to call in the HTML
        related_name="items",
        # when the todo list is deleted so will the item
        on_delete=models.CASCADE,
    )


# MAKE MIGRATION EVERY TIME MODEL IS CHANGED!
